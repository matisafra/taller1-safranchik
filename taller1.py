def Hazard(n):
    suma = 0 
    i = 1
    while i<=n:
        suma += (((-1)**(i+1))*2)/(2*i-1)
        i += 1
    return suma

def esPrimo(x):
    i = 1
    divisores = 0
    while i <= x:
        if x%i==0:
            divisores += 1
        i += 1
    if divisores == 2:
        return True

def es2N1(x):
    n = 0
    while (2**n)-1 <= x: 
        if (2**n) - 1 == x:
            return True
        n +=1
    return False

def lukaku(n):
    i=1
    count = 0
    while count < n: 
        if esPrimo(i) and es2N1(i):
            count += 1
        i += 1
    return i-1 #esto es por como definí el while: con i daría el siguiente

